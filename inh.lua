

do_multiparent_lookup = function (obj,key,o2)
  -- called as __index function, recursive, and from maybe_shared at __newindex
  local o3=obj;
  if (o2~=nil) then
    o3=o2;
  end
  local v = rawget(obj,key);
  local meta = getmetatable(obj);
  if (v~=nil) then
    -- obj raw-contains key, but check for private
    if (  (o3==obj)
    or (meta==nil)
    or (meta.__private==nil)
    or (meta.__private[key]==nil)  ) then
      return v,obj;
    else
      return nil;
    end
  end
  -- obj doesn't rawcontain key, continue with ascendants
  if (meta==nil) then
    return nil
  else
    local parents = meta.__parents;
    -- compatibility with single-ascendent in meta.__index
    if (parents==nil) then
      if (type(meta.__index)=="table") then
        parents = {meta.__index};
      end
    end
    if (parents==nil) then
      return nil;
    end
    -- stop search for container here, if obj has key marked private
    if (meta.__private~=nil) then
      if (meta.__private[key]~=nil) then
        return nil
      end
    end
    -- to the ascendants
    for _,p in ipairs(parents) do
      local pv,pv2 = do_multiparent_lookup(p,key,o3);
      if (pv~=nil) then
        return pv,pv2;
      end
    end
  end
  return nil
end


maybe_shared = function (obj,key,val)
  -- called on "newindex" for obj
  local v,container = do_multiparent_lookup(obj,key,obj);
  -- only modify a container, if it really marks the field as shared
  if (nil~=container) then
    local cm = getmetatable(container);
    if (cm==nil) then
      return rawset(obj,key,val);
    else
      local cms = cm.__shareindex;
      if ((cms==nil)or(cms[key]==nil)) then
        return rawset(obj,key,val);
      else
        return rawset(container,key,val);
      end
    end
  else 
    return rawset(obj,key,val);
  end
end

do_mark = function (obj,key,what)
  local meta = getmetatable(obj);
  if (meta==nil) then
    local meta = {};
    meta[what] = {};
    meta[what][key]=true;
    setmetatable(obj,meta);
  else
    if (meta[what]==nil) then
      meta[what]={};
    end;
    meta[what][key]=true;
  end;
end;

mark_shared = function (obj,key)
  do_mark(obj,key,"__shareindex");
end;

mark_private = function (obj,key)
  do_mark(obj,key,"__private");
end;



table_copy = function (t)
  local t2 = {}
  if (type(t)=="table") then
    for k,v in ipairs(t) do
      t2[k] = v
    end
  end
  return t2
end

do_inherit = function (x)
  local meta={};
  meta.__parents = table_copy(x);
  meta.__index=do_multiparent_lookup;
  meta.__newindex=maybe_shared;
  return setmetatable({},meta);
end

doinherit = function (...)
  return do_inherit({...});
end


return { 
  inherit=doinherit, 

  markprivate=function(obj,tab)
    for k,v in pairs(tab) do
      mark_private(obj,k);
    end
  end,
  
  markshared=function(obj,tab)
    for k,v in pairs(tab) do
      mark_shared(obj,k);
    end
  end,
  
  newinh=function(proto,init,priv,shared)
    local o=do_inherit(proto);
    if (type(init)=="table") then
      for k,v in pairs(init) do
        rawset(o,k,v);
      end
    end
    if (type(priv)=="table") then
      for k,v in pairs(priv) do
        mark_private(o,k);
      end
    end
    if (type(shared)=="table") then
      for k,v in pairs(shared) do
        mark_shared(o,k);
      end
    end
    return o;
  end
  
  
};


