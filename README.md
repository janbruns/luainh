# luainh



## Inheritance-Experiment with LUA

Here's the outcome of some interesting experimental coding around LUA inheritance.

It allows for prototype-style inheritance, with additional support for

- Multisource inheritance (the decendants can inherit from multiple ascendants)

- On a per field basis optional **sharing fields** along inheritance chain. That is, descendants of a proto may write prototype fields (using their very own view "self"), immediately visible from all other descendants

- Also supports private fields invisible to descendants (in the sense of namespace-pollution, only).



Usage is as follows

```
inh=require("inh");
new=inh.newinh;

Tvehicle=new( {}, --inherit
  {speed=130, horsepower=133}, --initial fields
  {horsepower=true}, --keys marked as private 
  nil   --keys marked as shared 
);
Tproduct=new(nil,{price=12345,failures=0.03, instances=11},nil,{instances="non-nil"} );

Tsomecar = new( { Tvehicle,Tproduct } );

function Tsomecar:drivehome (well) 
  print ( well .. " failures " .. self.failures)
  print (self.speed .. "mph @ " .. self.price .. "$.")
  self.instances=self.instances+173
  print ( self.instances .. " = " .. Tsomecar.instances .. " = " .. Tproduct.instances )
  print ( Tvehicle.horsepower .. "/" .. tostring(self.horsepower==nil) )
end;

Tsomecar:drivehome("Testdrive");
```
Where the drivehome() function shows some few references to global vars for verification purposes, only. The program prints
```
Testdrive failures 0.03
130mph @ 12345$.
184 = 184 = 184
133/true
```


## Project status
Project status is: Seems to work completely, but in no way production-use-ready (might have bugs difficult to notice in production).

## License
Nothing to talk about. Free software.


