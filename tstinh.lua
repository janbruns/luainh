
inh=require("inh");
new=inh.newinh;


Tvehicle=new(
  {}, --inherit
  {speed=130, horsepower=133}, --initial fields
  {horsepower=true}, --keys marked as private 
  nil   --keys marked as shared 
);
Tproduct=new(
  nil,--inherit
  {price=12345,failures=0.03, instances=11},--initial fields
  nil,--keys marked as private 
  {instances="non-nil"}  --keys marked as shared
);


Tsomecar = new(
  { Tvehicle,Tproduct }, --inherit
  nil,--initial fields
  nil,--keys marked as private 
  nil --keys marked as shared
);


function Tsomecar:drivehome (well) 
  print ( well .. " failures " .. self.failures)
  print (self.speed .. "mph @ " .. self.price .. "$.")
  self.instances=self.instances+173
  print ( self.instances .. " = " .. Tsomecar.instances .. " = " .. Tproduct.instances )
  print ( Tvehicle.horsepower .. "/" .. tostring(self.horsepower==nil) )
end;

Tsomecar:drivehome("Testdrive");

-- Output:
-- Testdrive failures 0.03
-- 130mph @ 12345$.
-- 184 = 184 = 184
-- 133/true
